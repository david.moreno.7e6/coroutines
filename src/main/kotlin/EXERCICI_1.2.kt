import kotlinx.coroutines.*

/*
Crea una funció doBackground que executi el codi de la corrutina creada amb launch al codi original.
 */

fun main() {
    println("The main program is started")
    GlobalScope.launch {
        doBackground()
    }
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}

suspend fun doBackground() = coroutineScope {
    println("Background processing started")
    delay(1000)
    println("Background processing finished")
}
