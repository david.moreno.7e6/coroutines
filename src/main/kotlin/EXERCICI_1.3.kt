import kotlinx.coroutines.*

/*
Modifica l’exercici anterior substituint el launch per withContext. Fes la resta de canvis que hagis de fer.
 */

suspend fun main() {
    println("The main program is started")
    withContext(Dispatchers.Default){
        doBackground()
    }
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}
