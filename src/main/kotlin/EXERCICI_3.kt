import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.File
import java.nio.file.Path
import kotlin.system.measureTimeMillis

/*
Crea una corrutina que reprodueixi una cançó. Entenem per “reproduir” anar mostrant la lletra per estrofes
d’una cançó amb 3 segons de retard entre línia i línia. La lletra es llegirà d’un arxiu.
L’aplicació principal mostrarà el temps transcorregut en l’execució de l’aplicació.
Per fer-ho, has d’incloure el codi que vulguis mesurar dins del següent fragment:
    time = measureTimeMillis {
       //TODO
    }
*/

fun main(){
    val time= measureTimeMillis {
        val song = File("./src/main/kotlin/cançó")
        val text= song.readLines()
        for (i in text){
            if (i != ""){
                runBlocking {
                    println(i)
                    delay(3000)
                }
            }
            else{
                println()
            }
        }
    }
    println()
    println("Duration: ${time/1000} seconds")
}

