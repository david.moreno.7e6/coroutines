import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.properties.Delegates

/*
Crea un programa que simula una cursa de cavalls.
Cada cavall serà una corrutina, que ha de completar un bucle de 4 iteracions
en que imprimirà un missatge indicant en quin punt del circuit està el cavall.
Cada missatge es mostrarà amb un delay aleatori. En acabar aquest bucle,
s’ha de mostrar un missatge indicant que el cavall ha acabat la cursa.
 */
var ranking= mutableListOf<Int>()
suspend fun main(){
    coroutineScope {
        launch {
            cavall1(1)
        }
        launch {
            cavall2(2)
        }
        launch {
            cavall3(3)
        }
        launch {
            cavall4(4)
        }
    }
    println()
    println("RANKING:")
    var position=0
    for (i in ranking){
        position++
        println("$position- Cavall numero $i")
    }

}

suspend fun cavall1(numero: Int){
    carrera(numero)
}
suspend fun cavall2(numero: Int){
    carrera(numero)
}
suspend fun cavall3(numero: Int){
    carrera(numero)
}

suspend fun cavall4( numero: Int){
    carrera(numero)
}

fun carrera( numero: Int){
    val missatges= listOf("El cavall amb numero $numero arranca la cursa", "Bon començament del cavall numero $numero", "S'apropa el final de la cursa per al cavall numero $numero", "Final per al cavall numero $numero")
    for (i in missatges){
        val numeroDelay= (1..5).random()
        runBlocking {
            delay(1000*numeroDelay.toLong())
            println(i)
        }
    }
    ranking.add(numero)
}