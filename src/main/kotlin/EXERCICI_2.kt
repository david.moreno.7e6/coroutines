import kotlinx.coroutines.*
import java.util.Scanner

val scanner= Scanner(System.`in`)

/*
Fes un programa en que la funció main cridi a una funció que escrigui per pantalla el missatge “Hello World!”
precedit pel número de missatge. Desprès d’escriure cada missatge espera 100 mil·lisegons.
En acabar la funció, torna al main que escriu per pantalla el missatge “Finished!”.
Fes dues versions del programa: una en que el launch es trobi al main i una altra dins de la funció.
 */

/*
OPCIÓ 1:
 */

//suspend fun helloWorld(input: Int)= coroutineScope {
//    var numero=0
//    for (i in 1..input){
//        launch {
//            delay(100)
//            println("World!")
//        }
//        delay(100)
//        numero++
//        print("$numero- Hello")
//    }
//}
//
//fun main()= runBlocking {
//    println("Introdueix el numero de vegades que vols visualitzar el missatge de benvinguda:")
//    val input= scanner.nextInt()
//    helloWorld(input)
//    delay(100)
//    println("Finished")
//}

/*
OPCIÓ 2:
 */

suspend fun helloWorld(input: Int)= coroutineScope {
    var numero=0
    for (i in 1..input){
        delay(100)
        numero++
        println("$numero- HelloWorld!")
    }
}

fun main()= runBlocking() {
    runBlocking {
        println("Introdueix el numero de vegades que vols visualitzar el missatge de benvinguda:")
        val input= scanner.nextInt()
        launch {
            helloWorld(input)
        }
    }
    delay(100)
    println("Finished")
}
