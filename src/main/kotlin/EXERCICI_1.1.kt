import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/*
Modifica el codi anterior de manera que el delay del runBlocking tingui un valor
més petit que el delay del launch. Quina sortida obtens?
*/

fun main() {
    println("The main program is started")
    GlobalScope.launch {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
    println("The main program continues")
    runBlocking {
        delay(500)
        println("The main program is finished")
    }
}

/*
RESPOSTA:

The main program is started
The main program continues
Background processing started
The main program is finished

EXPLICACIÓ:

Passa el mateix que a l'anterior codi amb la diferencia que els millis del runblocking son inferiors
que els del GlobalScope. Llavors es printea el runbocking i s'arriba al final del main i no es printea
ñ'ultim print del GlobalScope.

 */
