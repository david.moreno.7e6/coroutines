import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.Scanner

/*
Crea una aplicació en que l’usuari hagi d’encertar un número secret entre 1 i 50 que es generarà aleatòriament.
L’usuari anirà introduint valors fins que encerti el número.
L’aplicació tindrà un compte enrere de 10 segons. Passat aquest temps, l’aplicació finalitzarà.
 */

fun main(){
    val secretNumber= (1..50).random()
    var time= true
    runBlocking {
        println("3")
        delay(1000)
        println("2")
        delay(1000)
        println("1")
        delay(1000)
        println("Temps!")
    }
    GlobalScope.launch {
        delay(10000)
        time= false
        println("Temps finalitzat !")
    }
    while (time){
        println("Introdueix el numero:")
        val sc=Scanner(System.`in`)
        val input= sc.nextInt()
        if(input == secretNumber){
            println("Numero secret correcte!")
            time=false
        }
        else if(input >50 || input<1){
            println("Ha de ser un numero entre 1 i 50 !")
        }
        else{
            println("Incorrecte!")
        }
    }
}