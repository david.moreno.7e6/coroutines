/*
· Quina sortida generarà el següent codi?

fun main() {
   println("The main program is started")
   GlobalScope.launch {
       println("Background processing started")
       delay(1000)
       println("Background processing finished")
   }
   println("The main program continues")
   runBlocking {
       delay(1500)
       println("The main program is finished")
   }
}


RESPOSTA:

The main program is started
The main program continues
Background processing started
Background processing finished
The main program is finished


EXPLICACIÓ:

Executa el primer print i arriba al GlobalScope. El GlobalScope es fa
la vegada que el següent print. Llavors es printea el següent print y després
es printea el globalScope. Com els millis son inferiors als millis del runblocking,
es printea abans l'ultim print del GlobalScope que el del runblocking.
 */